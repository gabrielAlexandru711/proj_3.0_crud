<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     *@Route("/user", name="user", methods={"POST"})
     */
    public function User(Request $request)
    {
        if($request->request->has('username')) {
            $username = $request->request->get('username');
        } else {
            return new response('Introduceti username-ul', 400);
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $username]);

        if ($user) {
            return new response('Acest username este deja folosit: ' . $username, 404);
        }

        if(!$request->request->has('role')) {
            $role = 'ROLE_USER';
        } else {
            $role = $request->request->get('role');
            if($role != 'ROLE_ADMIN' && $role != 'ROLE_USER'){
                return new response('Rolul este gresit', 400);
            }
        }

        $apiToken = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');

        $entityManager = $this->getDoctrine()->getManager();
        $user = new User();
        $user->setUserName($username);
        $user->setApiToken($apiToken);
        $user->setRoles([$role]);

        $entityManager->persist($user);

        $entityManager->flush();

        return new Response();
    }
}

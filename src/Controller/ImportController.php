<?php

namespace App\Controller;

use App\Entity\Form;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportController extends AbstractController
{
    /**
     * @Route("/import", name="import", methods={"POST"})
     */
    public function index(Request $request, ValidatorInterface $validator)
    {
        $file = $request->files->get('file');
        $data = file_get_contents($file->getPathname());

        $encoders = [new CsvEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);


        $content = $serializer->decode($data,'csv');

        foreach ($content as $item) {
            $entityManager = $this->getDoctrine()->getManager();

            $user = new Form();

            foreach ($item as $key => $data) {
                $func = 'set' . ucfirst($key);
                $user->$func($data);
            }
            $errors = $validator->validate($user);
            if (count($errors) > 0) {
                $messages = [];
                foreach ($errors as $violation) {
                    $messages[$violation->getPropertyPath()][] = $violation->getMessage();
                }
                return new Response(json_encode($messages));
            }

            $entityManager->persist($user);

            $entityManager->flush();
        }

        return new Response('done');
    }
}

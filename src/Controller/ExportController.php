<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\User;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExportController extends AbstractController
{
    /**
     * @Route("/export", name="export", methods={"GET"})
     */
    public function index(SerializerInterface $serializer, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('u')->from('App:Form', 'u');

        if($request->query->get('nume')){
            $qb->andWhere('u.nume = ?1')->setParameter(1, $request->query->get('nume'));
        }
        if($request->query->get('prenume')){
            $qb->andWhere('u.prenume = ?2')->setParameter(2, $request->query->get('prenume'));
        }
        if($request->query->get('sex')){
            $qb->andWhere('u.sex = ?3')->setParameter(3, $request->query->get('sex'));
        }
        if($request->query->get('adresa')){
            $qb->andWhere('u.adresa = ?4')->setParameter(4, $request->query->get('adresa'));
        }
        if($request->query->get('oras')){
            $qb->andWhere('u.oras = ?5')->setParameter(5, $request->query->get('oras'));
        }
        if($request->query->get('email')){
            $qb->andWhere('u.email = ?6')->setParameter(6, $request->query->get('email'));
        }
        if($request->query->get('id')){
            $qb->andWhere('u.id = ?7')->setParameter(7, $request->query->get('id'));
        }

        if($request->query->get('format')){
            $format = $request->query->get('format');
            if($format == 'xlsx'){
                $query = $qb->getQuery();
                $result = $query->getArrayResult();

                $encoders = [new CsvEncoder()];
                $normalizers = [new ObjectNormalizer()];

                $serializer = new Serializer($normalizers, $encoders);

                $content = $serializer->serialize($result, 'csv');

                $one=explode(chr(10),$content);
                $array = array();
                foreach ($one as $item){
                    $array[] = explode(",",$item);
                }

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet()->fromArray($array);
                $sheet->setTitle('export');

                $writer = IOFactory::createWriter($spreadsheet, "Xlsx");

                ob_start();
                $writer->save('php://output');
                $excelOutput = ob_get_clean();


                return new Response(
                    $excelOutput,
                    200,
                    [
                        'content-type'        =>  'text/x-csv; charset=windows-1251',
                        'Content-Disposition' => 'attachment; filename="export.xlsx"',
                    ]
                );
            }
        } else {
            return new Response('format lipsa', 400);
        }

        $query = $qb->getQuery();

        $result = $query->getArrayResult();

        $encoders = [new XmlEncoder(), new JsonEncoder(), new CsvEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $content = $serializer->serialize($result, $format);

        $response =  new Response($content);

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'export.' . $format
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}

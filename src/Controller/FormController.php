<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Form;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FormController extends AbstractController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     */
    public function list(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('u')->from('App:Form', 'u');

        if($request->query->get('nume')){
            $qb->andWhere('u.nume = ?1')->setParameter(1, $request->query->get('nume'));
        }
        if($request->query->get('prenume')){
            $qb->andWhere('u.prenume = ?2')->setParameter(2, $request->query->get('prenume'));
        }
        if($request->query->get('sex')){
            $qb->andWhere('u.sex = ?3')->setParameter(3, $request->query->get('sex'));
        }
        if($request->query->get('adresa')){
            $qb->andWhere('u.adresa = ?4')->setParameter(4, $request->query->get('adresa'));
        }
        if($request->query->get('oras')){
            $qb->andWhere('u.oras = ?5')->setParameter(5, $request->query->get('oras'));
        }
        if($request->query->get('email')){
            $qb->andWhere('u.email = ?6')->setParameter(6, $request->query->get('email'));
        }
        if($request->query->get('id')){
            $qb->andWhere('u.id = ?7')->setParameter(7, $request->query->get('id'));
        }

        $query = $qb->getQuery();

        $result = $query->getArrayResult();

        return new Response(json_encode($result));
    }

    /**
     * @Route("/insert", name="insert", methods={"POST"})
     */
    public function insert(Request $request, ValidatorInterface $validator)
    {
        $setData = array('sex'=>'', 'nume'=>'', 'prenume'=>'', 'oras'=>'', 'email'=>'', 'accord'=>'');
        $data = $request->request->all();

        $setData = array_merge($setData, $data);
        $entityManager = $this->getDoctrine()->getManager();

        $user = new Form();

        foreach ($setData as $key => $data){
            $func = 'set'.ucfirst($key);
            $user->$func($data);
        }
        $errors = $validator->validate($user);
        if (count($errors) >0) {
            $messages = [];
            foreach ($errors as $violation) {
                $messages[$violation->getPropertyPath()][] = $violation->getMessage();
            }
            return new Response(json_encode($messages));
        }

        $entityManager->persist($user);

        $entityManager->flush();
        return new Response($user->getId());
    }

    /**
     * @Route("/delete", name="delete", methods={"POST"})
     */
    public function delete(Request $request)
    {
        if($request->request->has('id')) {
            $id = $request->request->get('id');
        } else {
            return new response('Introduceti id-ul', 400);
        }

        $user = $this->getDoctrine()->getRepository(Form::class)->find($id);

        if (!$user) {
            return new response('Nu exista inregistrare cu id: ' . $id, 404);
        }

        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();

        return new Response('Inregistrarea a fost stearsa');
    }

    /**
     * @Route("/update", name="update", methods={"POST"})
     */
    public function update(Request $request)
    {
        $data = $request->request->all();

        if(!array_key_exists('id', $data)) {
            return new response('Introduceti id-ul', 400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(Form::class)->find($data['id']);

        if (!$user) {
            return new response('Nu exista inregistrare cu id: ' . $data['id'], 404);
        }

        foreach ($data as $key => $value){
            if($key != 'id') {
                $func = 'set' . ucfirst($key);
                $user->$func($value);
            }
        }

        $entityManager->flush();

        return new Response('Inregistrarea a fost modificata');
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Image;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImageController extends AbstractController
{
    /**
     * @Route("/image/upload", name="image-upload", methods={"POST"})
     */
    public function upload(Request $request, ValidatorInterface $validator)
    {
        $file = $request->files->get('img');
        $apiToken = $request->headers->get('X-AUTH-TOKEN');

        $repository = $this->getDoctrine()->getRepository(User::class);
        $entityManager = $this->getDoctrine()->getManager();

        $user = $repository->findOneBy(['apiToken' => $apiToken]);

        $image = new Image();
        $image->setFile($file);
        $image->setUserId($user->getId());
        $image->setName($file->getClientOriginalName());

        $errors = $validator->validate($image);
        if (count($errors) > 0) {
            $messages = [];
            foreach ($errors as $violation) {
                $messages[$violation->getPropertyPath()][] = $violation->getMessage();
            }
            return new Response(json_encode($messages));
        }

        $entityManager->persist($image);
        $entityManager->flush();

        return new Response("imagine incarcata cu succes");
    }

    /**
     * @Route("/image/get", name="image-get", methods={"GET"})
     */
    public function getImage(Request $request)
    {
        $apiToken = $request->headers->get('X-AUTH-TOKEN');

        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneBy(['apiToken' => $apiToken]);

        $entityManager = $this->getDoctrine()->getManager();
        $qb = $entityManager->createQueryBuilder()
            ->select('i')
            ->from('App:Image', 'i')
            ->andWhere('i.user_id = ?1')
            ->setParameter(1, $user->getId())
            ->orderBy("i.id", "DESC")->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if(!$qb){
            return new Response('utilizatorul nu are imagine');
        }

        $file = $qb->getFile();
        $fileName = $qb->getName();

        $response = new BinaryFileResponse($file->getPathname());

        $response->headers->set('Content-Type', $file->getMimeType());

        $response->setContentDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $fileName
        );

        return $response;
    }
}

<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Email extends Constraint
{
    public $message = 'Email-ul "{{ value }}" nu mai poate fi folosit.';
    public $x;
}
